const { json } = require('body-parser');
const { request, application } = require('express');
const res = require('express/lib/response');

module.exports = function (app, fs) {
    const JSONdb = require('simple-json-db');
    const userdb = new JSONdb('database.json'); //simple json db로 database.json사용
    const postdb = new JSONdb('post.json'); //simple json db로 post.json사용


    //index(로그인)
    app.get('/', function (req, res) {
        app.locals.user = userdb.get('user');
        res.render('index');
        /*
        let email = req.body;

        let uesr = userdb.get('user');
        let map = uesr.map(x => {
            if (x.email == email) { //post에서 id가 url로 받아온 id와 동일하다면
                let userdata = user.filter((user) => user.email == email)
            } return x;
        });
        res.json(map);
        */
    });

    //view
    app.get('/view', function (req, res) {
        app.locals.movie = postdb.get('movie');
        res.render('view');
    });

    //viewinfo
    app.get('/get/:id', function (req, res) {
        const user = userdb.get('user');
        app.locals.user = user;
        const post = postdb.get('movie');
        const id = req.params.id;
        const movie = post.filter((post) => post.id == id);  //post에서 id가 url로 받아온 id와 동일하다면 movie에 저장
        app.locals.movie = movie;
        res.render('viewinfo');
    });

    //sign
    app.get('/sign', function (req, res) {
        res.render('sign');
    });

    //info
    app.get('/info', function (req, res) {
        app.locals.user = userdb.get('user');
        res.render("info");
    });

    //wirte
    app.get('/write', function (req, res) {
        app.locals.movie = postdb.get('movie');
        res.render('write');
    });

    //edit
    app.get('/edit/:id', function (req, res) {
        const movie = postdb.get('movie');
        app.locals.id = req.params.id;
        app.locals.movie = movie;
        res.render('edit');
    });

    //post api
    app.post('/addPost', function (req, res) {
        const post = postdb.get('movie');
        const postdata = req.body;
        postdata.comment = [];
        post.push(postdata);
        postdb.set('movie', post);
    });

    //delete api
    app.delete('/deletePost/:id', function (req, res) {
        const post = postdb.get('movie');
        const id = req.params.id;
        const deletepost = post.filter((post) => post.id !== id);
        postdb.set('movie', deletepost);
    });

    //put api
    app.put('/editPost/:id', function (req, res) {
        const post = postdb.get('movie');
        const id = req.params.id;
        const edit = req.body;

        const map = post.map(x => {
            if (x.id == id) { //post에서 id가 url로 받아온 id와 동일하다면
                x.title = edit.title; //post의 title, article, date, poster에 req.body로 받아온 데이터로 새롭게 저장
                x.article = edit.article;
                x.date = edit.date;
                x.poster = edit.poster;
            } return x;
        });
        postdb.set('movie', map);
    });

    //post api, comment
    app.post('/postComment/:id', function (req, res) {
        const post = postdb.get('movie');
        const id = req.params.id;
        const commentdata = req.body;
        var comment = {};
        comment = post[id].comment;
        comment.push(commentdata);

        const map = post.map(x => {
            if (x.id == id) { //post에서 id가 url로 받아온 id와 동일하다면
                x.comment = comment; //post의 comment에 req.body로 받아온 데이터로 새롭게 저장
            } return x;
        });
        postdb.set('movie', map);
        //res.json(commentdata)
    });


    //delete api, comment
    app.delete('/deleteComment/:id', function (req, res) {
        const post = postdb.get('movie');
        const id = req.params.id;
        const commentid = req.query.commentid;
        const post2 = post[id].comment;

        const deletepost = post2.filter((post2) => post2.id !== commentid);

        const map = post.map(x => {
            if (x.id == id) {
                x.comment = deletepost;
            } return x;
        })
        postdb.set('movie', map);
        //res.json(map);
    });


    //put api, comment
    app.put('/editComment/:id', function (req, res) {
        const post = postdb.get('movie');
        const id = req.params.id;
        const commentid = req.query.commentid;
        const edit = req.body;

        const map = post.map(x => {
            if (x.id == id) {
                x.comment[commentid].comment = edit.comment;
                x.comment[commentid].cdate = edit.cdate;
            } return x;
        });
        postdb.set('movie', map);
    });

    //post api, user
    app.post('/addUser', function (req, res) {
        const user = userdb.get('user');
        const userdata = req.body;
        user.push(userdata);
        userdb.set('user', user);
    });
}