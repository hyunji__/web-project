var express = require('express');
var app = express();

var bodyParser = require('body-parser');
var fs = require("fs");

app.set('views', __dirname + '/views'); //views 위치, __dirname=현재 실행 중인 폴더 경로
app.set('view engine', 'ejs'); //ejs 사용
app.engine('html', require('ejs').renderFile); //ejs 사용
app.use('/public', express.static('public')); //정적 파일 사용

app.use(bodyParser.json());
app.use(bodyParser.urlencoded());

var server = app.listen(3000, function () { //localhost 3000 사용
    console.log("server has started on port 3000")
});

var router = require('./router/main')(app, fs); //router의 main.js 불러오기